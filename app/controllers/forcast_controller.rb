# include HTTParty
API_KEY = 'f2fc7abe88c7ddfab97ab11172f362e5'
URL = "http://api.openweathermap.org/data/2.5/forecast/daily?&cnt=16&APPID="+API_KEY+"&q="


class ForcastController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index

    @forcastData = HTTParty.get URL.to_s+params[:city]
    @body = JSON.parse(@forcastData.body)


    @futureForcast = []
    @defaultForcast = []
    @pastForcast = []
    defaultDate = params[:date] ? Integer(params[:date]) : @body['list'][0]['dt']

    @body['list'].each_with_index do |post, index|


      val = post
      val[:timestamp] = post['dt']
      val[:date] = Time.at(post['dt']).strftime('%A %d')
      # val[:avgTemprature]= ((post['temp']['day'] + post['temp']['min'] + post['temp']['max'] + post['temp']['night'] + post['temp']['eve'] + post['temp']['morn'])/6).round(2)
      val[:avgTemprature]= post['temp']['day'].round(2)

      if post['dt'] > defaultDate then
        @futureForcast.push(val)
      elsif  post['dt'] < defaultDate then
        @pastForcast.push(val)
      else
        @defaultForcast = val
      end



    end

    # hash = JSON.parse @forcastData.body.to_s

    # render :layout => false
  end
  def forcastDetail



  end
end
