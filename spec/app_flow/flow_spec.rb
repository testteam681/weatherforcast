require "rails_helper"

describe "The website flow", :type => :feature do
  it "user uses site" do
    visit '/'
    fill_in 'city', :with => 'London'

    click_button 'Fetch'

    expect(page).to have_content 'London'
    page.has_css?('weatherwidget')
    # within(".weatherwidget") do
    # end
    Capybara.match = :first
    click_link('day')
    expect(page).to have_content 'London'
    page.has_css?('weatherwidget')
  end
end
